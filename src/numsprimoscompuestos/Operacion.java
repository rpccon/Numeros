/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package numsprimoscompuestos;

import java.util.LinkedList;
import java.util.Scanner;

/**
 *
 * @author Johanny
 */
public class Operacion {
    Scanner user_input ;
    int[] numeros;
    
    public Operacion(){
        user_input = new Scanner( System.in );
        numeros = new int[4];
        
    }
    
    
    public void  Inicio(){
        pMsj("Indique 4 números...");
        
        try{
            for(int i=0;i<4;i++){
                pMsj("----->Ingrese el número "+String.valueOf(i+1));
                String num = user_input.next();
                int nuevoNum = Integer.valueOf(num);
                //pMsj("Got: "+String.valueOf(nuevoNum));
                
                numeros[i]=nuevoNum;
                
            }     
            Calculo();
        }
        catch(Exception e){
            
        }

    }
    public void Calculo(){
        pMsj("Los números ingresados son: \n");
        imprimirNumeros();
        mayorMenor();
        pMsj("Los números ordenados de mayor a menor son: \n");
        imprimirNumeros();
        desfragmentarPrimosComp();
        
    }
    
    public void desfragmentarPrimosComp(){
        String bufferA = "Se sumó: \n------->";
        String bufferB = "Se multiplicó: \n------->";
        int resultA=0;
        int resultB=1;
        for(int i = 0; i < numeros.length; i++){
            if(buscaPrimo(numeros[i])==true){
                bufferB+=String.valueOf(numeros[i])+" ";

                resultB=resultB*numeros[i];
                
            }
            else{
                bufferA+=String.valueOf(numeros[i])+" ";


                resultA=resultA+numeros[i];
            }
        }
        bufferA+="="+String.valueOf(resultA);
        bufferB+="="+String.valueOf(resultB);
        pMsj(bufferB);
        pMsj(bufferA);
    }
    
    public void imprimirNumeros(){
        String buffer = "";
        for(int i=0;i<numeros.length;i++){
            buffer+="\n"+String.valueOf(i+1)+" --> "+String.valueOf(numeros[i]);
        }
        buffer+="\n";
        pMsj(buffer);
        
    }
    
    public void mayorMenor(){
        int aux = 0;
	for(int i = 0; i < numeros.length; i++){
            for(int j=i+1; j < numeros.length; j++){
		if(numeros[i] < numeros[j]){
                    aux = numeros[i];
                    numeros[i] = numeros[j];
                    numeros[j] = aux;
		}
            }
	}
    }
    
    public boolean buscaPrimo(int numero){
        int contador = 2;
        boolean primo=true;
        while ((primo) && (contador!=numero)){
          if (numero % contador == 0)
            primo = false;
          contador++;
        }
        return primo;
    }
    public void Regresar(){
        System.out.println("Presione cualquier tecla y enter para regresar a menu.....");
        String num = user_input.next();
        Inicio();
    }
    
    public void pMsj(String mensaje){// función para imprimir mensaje en consola más eficientemente
        System.out.println(mensaje);
    }
}
